using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Abstract;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IRepository<Employee> employeeRepository;

        public EmployeesService(IRepository<Employee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public async Task<ServiceResponse<bool>> UpdateAppliedPromocodesAsync(Guid id)
        {
            var responce = new ServiceResponse<bool>();

            var employee = await employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                responce.Message = "Not found";
                responce.Success = false;
                return responce;
            }

            employee.AppliedPromocodesCount++;

            await employeeRepository.UpdateAsync(employee);

            responce.Success = true;
            responce.Data = true;
            responce.Message = "Ok";

            return responce;
        }
    }
}