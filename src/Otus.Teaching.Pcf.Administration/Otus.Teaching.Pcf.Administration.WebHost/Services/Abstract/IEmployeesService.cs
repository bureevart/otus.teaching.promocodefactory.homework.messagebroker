using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.WebHost.Services.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.Abstract
{
    public interface IEmployeesService
    {
        Task<ServiceResponse<bool>> UpdateAppliedPromocodesAsync(Guid id);
    }
}
