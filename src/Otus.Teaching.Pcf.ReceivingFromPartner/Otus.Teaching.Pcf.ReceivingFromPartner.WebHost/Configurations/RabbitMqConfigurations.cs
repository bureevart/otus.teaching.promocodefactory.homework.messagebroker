namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Configurations
{
    public class Rabbit
    {
        public string Uri { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}