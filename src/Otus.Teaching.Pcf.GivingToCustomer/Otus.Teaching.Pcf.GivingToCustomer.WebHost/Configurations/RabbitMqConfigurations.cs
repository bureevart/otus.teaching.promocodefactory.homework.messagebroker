namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Configurations
{
    public class Rabbit
    {
        public string Uri { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}