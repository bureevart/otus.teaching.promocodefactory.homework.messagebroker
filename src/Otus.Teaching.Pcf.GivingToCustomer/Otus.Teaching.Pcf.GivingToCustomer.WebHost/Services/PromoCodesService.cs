using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstract;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class PromoCodesService : IPromoCodesService
    {
        private readonly IRepository<PromoCode> promoCodesRepository;
        private readonly IRepository<Preference> preferencesRepository;
        private readonly IRepository<Customer> customersRepository;
        private readonly ILogger<PromoCodesService> logger;

        public PromoCodesService(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository,
            ILogger<PromoCodesService> logger)
        {
            this.promoCodesRepository = promoCodesRepository;
            this.preferencesRepository = preferencesRepository;
            this.customersRepository = customersRepository;
            this.logger = logger;
        }

        public async Task<ServiceResponse<bool>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO givePromoCodeDTO)
        {
            var response = new ServiceResponse<bool>();
            var preference = await preferencesRepository.GetByIdAsync(givePromoCodeDTO.PreferenceId);

            if (preference == null)
            {
                response.Success = false;
                response.Message = "Not found";
                return response;
            }

            var customers = await customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var promoCode = PromoCodeMapper.MapFromDTO(givePromoCodeDTO, preference, customers);

            await promoCodesRepository.AddAsync(promoCode);

            response.Success = true;
            response.Message = "Ok";
            response.Data = true;

            return response;
        }
    }
}