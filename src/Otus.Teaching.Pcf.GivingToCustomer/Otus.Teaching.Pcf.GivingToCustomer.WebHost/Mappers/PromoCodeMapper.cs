﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.IntegrationEvents;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromDTO(GivePromoCodeDTO givePromoCodeDTO, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode();
            promocode.Id = givePromoCodeDTO.PromoCodeId;
            
            promocode.PartnerId = givePromoCodeDTO.PartnerId;
            promocode.Code = givePromoCodeDTO.PromoCode;
            promocode.ServiceInfo = givePromoCodeDTO.ServiceInfo;
           
            promocode.BeginDate = DateTime.Parse(givePromoCodeDTO.BeginDate);
            promocode.EndDate = DateTime.Parse(givePromoCodeDTO.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}
