using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.Contracts;
using Otus.Teaching.Pcf.IntegrationEvents;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstract;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Integration.EventHandling
{
    public class PromoCodeToCustomerRequestedEventHandler : IConsumer<PromoCodeToCustomerRequestedEvent>
    {
        private readonly IPromoCodesService promoCodesService;

        public PromoCodeToCustomerRequestedEventHandler(IPromoCodesService promoCodesService)
        {
            this.promoCodesService = promoCodesService;
        }
        public async Task Consume(ConsumeContext<PromoCodeToCustomerRequestedEvent> context)
        {
            var promoDto = new GivePromoCodeDTO()
            {
                ServiceInfo = context.Message.ServiceInfo,
                PartnerId = context.Message.PartnerId,
                PromoCodeId = context.Message.PromoCodeId,
                PromoCode = context.Message.PromoCode,
                PreferenceId = context.Message.PreferenceId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate
            }; 
            await promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(promoDto);
        }
    }
}